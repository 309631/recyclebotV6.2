//Screen Updates - what the user will see at a given point in menu, given the position and temperature
//only updated when on the home screen or changes are made to the system.

void screenUpdate(int state, double currentTemp) {
  
  lastInputTime = millis(); 
  lcd.setCursor(0,0);
  lcd.clear();

	auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  
  switch (state) {
    case 0 :
      infoScreen(currentTemp);
      break;
    case 1 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Manual");
      lcd.setCursor(0,2);
      lcd.print("   Auto");
      break;
    case 2 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Manual");
      lcd.setCursor(0,2);
      lcd.print("   Auto");
      break;
    case 3 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Cooling");
      lcd.setCursor(0,2);
      lcd.print("   Filament Guide");
      lcd.setCursor(0,3);
      lcd.print("   Puller");
      break;
    case 4 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Cooling");
      lcd.setCursor(0,2);
      lcd.print("   Filament Guide");
      lcd.setCursor(0,3);
      lcd.print("   Puller");
      break;
      /*
    case 5 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Fan");
      lcd.setCursor(0,2);
      lcd.print("   Water Bath");
      break;
    case 6 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Fan Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 7 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 8 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
      */
    case 9 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Speed");
      break;
      /*
    case 10 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Fan");
      lcd.setCursor(0,2);
      lcd.print("-> Water Bath");
      break;
    case 11 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Bath Power");
      break;
    case 12 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 13 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
      */
    case 14 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Cooling");
      lcd.setCursor(0,2);
      lcd.print("-> Filament Guide");
      lcd.setCursor(0,3);
      lcd.print("   Puller");
      break;
    case 15 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 16 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Spool Width");
      lcd.setCursor(0,2);
      lcd.print("   Position");
      lcd.setCursor(0,3);
      lcd.print("   Filament Diameter");
      break;
    case 17 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Start");
      lcd.setCursor(0,2);
      lcd.print("   Stop");
      break;
    case 18 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Start");
      lcd.setCursor(0,2);
      lcd.print("   Stop");
      break;
    case 19 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Start");
      lcd.setCursor(0,2);
      lcd.print("-> Stop");
      break;
    case 20 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Spool Width");
      lcd.setCursor(0,2);
      lcd.print("-> Position");
      lcd.setCursor(0,3);
      lcd.print("   Filament Diameter");
      break;
    case 21 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Spool Width");
      lcd.setCursor(0,2);
      lcd.print("   Position");
      lcd.setCursor(0,3);
      lcd.print("-> Filament Diameter");
      break;
    case 22 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Cooling");
      lcd.setCursor(0,2);
      lcd.print("   Filament Guide");
      lcd.setCursor(0,3);
      lcd.print("-> Puller");
      break;
    case 23 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Puller Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 24 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Puller Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 25 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 26 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 27 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
    case 28 :
      lcd.print("   Cooling");
      lcd.setCursor(0,1);
      lcd.print("   Filament Guide");
      lcd.setCursor(0,2);
      lcd.print("   Puller");
      lcd.setCursor(0,3);
      lcd.print("-> Auger");
      break;
    case 29 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Auger Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 30 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Auger Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 31 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 32 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 33 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
    case 34 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Auger Power");
      lcd.setCursor(0,2);
      lcd.print("-> Speed");
      break;
    case 35 :
      lcd.print("   Filament Guide");
      lcd.setCursor(0,1);
      lcd.print("   Puller");
      lcd.setCursor(0,2);
      lcd.print("   Auger");
      lcd.setCursor(0,3);
      lcd.print("-> Winder");
      break;
    case 36 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Winder Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 37 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Winder Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 38 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 39 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 40 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
    case 41 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Winder Power");
      lcd.setCursor(0,2);
      lcd.print("-> Speed");
      break;
    case 42 :
      lcd.print("   Puller");
      lcd.setCursor(0,1);
      lcd.print("   Auger");
      lcd.setCursor(0,2);
      lcd.print("   Winder");
      lcd.setCursor(0,3);
      lcd.print("-> Heater");
      break;
    case 43 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Heater Power");
      lcd.setCursor(0,2);
      lcd.print("   Temperature");
      break;
    case 44 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Heater Power");
      lcd.setCursor(0,2);
      lcd.print("   Temperature");
      break;
    case 45 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 46 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
    case 47 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
    case 48 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Heater Power");
      lcd.setCursor(0,2);
      lcd.print("-> Temperature");
      break;
    case 49 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Manual");
      lcd.setCursor(0,2);
      lcd.print("-> Auto");
      break;
      /*
    case 50 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Auto On");
      lcd.setCursor(0,2);
      lcd.print("   Auto Off");
      break;
    case 51 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Plastic Type");
      lcd.setCursor(0,2);
      lcd.print("   Diameter");
      lcd.setCursor(0,3);
      lcd.print("   Spool Width");
      break;
    case 52 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Regular PLA");
      lcd.setCursor(0,2);
      lcd.print("   Recycled PLA");
      lcd.setCursor(0,3);
      lcd.print("   Recycled ABS");
      break;
    case 53 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Regular PLA");
      lcd.setCursor(0,2);
      lcd.print("   Recycled PLA");
      lcd.setCursor(0,3);
      lcd.print("   Recycled ABS");
      break;
    case 54 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Regular PLA");
      lcd.setCursor(0,2);
      lcd.print("-> Recycled PLA");
      lcd.setCursor(0,3);
      lcd.print("   Recycled ABS");
      break;
    case 55 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Regular PLA");
      lcd.setCursor(0,2);
      lcd.print("   Recycled PLA");
      lcd.setCursor(0,3);
      lcd.print("-> Recycled ABS");
      break;
    case 56 :
      lcd.print("   Regular PLA");
      lcd.setCursor(0,1);
      lcd.print("   Recycled PLA");
      lcd.setCursor(0,2);
      lcd.print("   Recycled ABS");
      lcd.setCursor(0,3);
      lcd.print("-> Recycled TPE");
      break;
    case 57 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Plastic Type");
      lcd.setCursor(0,2);
      lcd.print("-> Diameter");
      lcd.setCursor(0,3);
      lcd.print("   Spool Width");
      break;
    case 58 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Plastic Type");
      lcd.setCursor(0,2);
      lcd.print("-> Diameter");
      lcd.setCursor(0,3);
      lcd.print("   Spool Width");
      break;
    case 59 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Start");
      lcd.setCursor(0,2);
      lcd.print("   Stop");
      break;
    case 60 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Start");
      lcd.setCursor(0,2);
      lcd.print("   Stop");
      break;
    case 61 :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Start");
      lcd.setCursor(0,2);
      lcd.print("-> Stop");
      break;
      */
    case 62 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Speed");
      break;
      /*
    case 63 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Fan Power");
      lcd.setCursor(0,2);
      lcd.print("   Speed");
      break;
    case 64 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
      
      
    case 65 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   Bath Power");
      break;
    case 66 :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
      */
    case 67 :
      lcd.print("Fan Speed");
      lcd.setCursor(0,1);
       lcd.print(encoderPos);
       lcd.print(" %");
     break;
    case 68 :
      lcd.print("Spool Start pos");
        lcd.setCursor(0,1);
        lcd.print(encoderPos);
        //filamentGuide.write(encoderPos);
      break;
    case (69) :
      lcd.print("Spool Stop Pos");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
     //filamentGuide.write(encoderPos);
     break;
    case (70) :
      lcd.print("Filament Guide Position");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      
      //filamentGuide.write(encoderPos);
      break;
    case (71) :
      lcd.print("-> Back");
      lcd.setCursor(0,1);
      lcd.print("   1.75 mm");
      lcd.setCursor(0,2);
      lcd.print("   3.0 mm");
      break;
    case (72) :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> 1.75 mm");
      lcd.setCursor(0,2);
      lcd.print("   3.0 mm");
      break;
    case (73) :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   1.75 mm");
      lcd.setCursor(0,2);
      lcd.print("-> 3.0 mm");
      break;
    case (74) :
      lcd.print("Auger Speed");
      
      lcd.setCursor(0,1);
      
      lcd.print(encoderPos);
      //.print(" RPM");
      break;
    case (75) :
      lcd.print("Winder Speed");
      lcd.print(encoderPos);
      lcd.print(" RPM");
      break;
    case (76) :
      lcd.print("Heater Temp");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" C");
      break;
    case (77) :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Puller Power");
      lcd.setCursor(0,2);
      lcd.print("-> Speed");
      break;
    case (78) :
      lcd.print("Puller Speed");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" RPM");
      break;
    case (79) :
      lcd.print("Set Spool Width and");
      lcd.setCursor(0,1);
      lcd.print("Filament Diameter");
      lcd.setCursor(0,2);
      lcd.print("Before Operation");
      break;
    case (80) :
      lcd.print("Spool Start");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      break;
    case (81) :
      lcd.print("Spool Stop");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      break;
    case (82) :
      lcd.print("Filament Diameter");
      lcd.setCursor(0,1);
      lcd.print("-> 1.75 mm");
      lcd.setCursor(0,2);
      lcd.print("   3.0 mm");
      break;
      case (83) :
      lcd.print("Filament Diameter");
      lcd.setCursor(0,1);
      lcd.print("   1.75 mm");
      lcd.setCursor(0,2);
      lcd.print("-> 3.0 mm");
      break;
      /*
      case (84) :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("-> Auto On");
      lcd.setCursor(0,2);
      lcd.print("   Auto Off");
      break;
      case (85) :
      lcd.print("   Back");
      lcd.setCursor(0,1);
      lcd.print("   Auto On");
      lcd.setCursor(0,2);
      lcd.print("-> Auto Off");
      break;
      */
     
     
     
     
     
     
     
     
     
     case (86) :
      lcd.print("Load Plastic Type");
      lcd.setCursor(0,1);
      lcd.print("-> PLA");
      lcd.setCursor(0,2);
      lcd.print("   ABS");
      lcd.setCursor(0,3);
      lcd.print("   PET");
      break;
      case (87) :
      lcd.print("Load Plastic Type");
      lcd.setCursor(0,1);
      lcd.print("   PLA");
      lcd.setCursor(0,2);
      lcd.print("-> ABS");
      lcd.setCursor(0,3);
      lcd.print("   PET");
      break;
      case (88) :
      lcd.print("Load Plastic Type");
      lcd.setCursor(0,1);
      lcd.print("   PLA");
      lcd.setCursor(0,2);
      lcd.print("   ABS");
      lcd.setCursor(0,3);
      lcd.print("-> PET");
      break;
      case (89) :
      lcd.print("Load Plastic Type");
      lcd.setCursor(0,1);
      lcd.print("   ABS");
      lcd.setCursor(0,2);
      lcd.print("   PET");
      lcd.setCursor(0,3);
      lcd.print("-> TPU");
      break;
      case (90) :
      lcd.print("Load Plastic Type");
      lcd.setCursor(0,1);
      lcd.print("   PET");
      lcd.setCursor(0,2);
      lcd.print("   TPU");
      lcd.setCursor(0,3);
      lcd.print("-> Custom");
      break;
     case (91) :
      lcd.print("Heater Temp");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" C");
      break;
      case (92) :
      lcd.print("Auger Speed");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      //lcd.print(" RPM");
      break;
      case (93) :
      lcd.print("Puller Speed");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" RPM");
      break;
      case (94) :
      lcd.print("Winder Speed");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" RPM");
      break;
      case (95) :
      lcd.print("Cooling Power");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      lcd.print(" %");
      break;
    case (96) :
      lcd.print("Automatic Mode");
      lcd.setCursor(0,1);
      lcd.print("-> On");
      lcd.setCursor(0,2);
      lcd.print("   Off");
      break;
      case (97) :
      lcd.print("Automatic Mode");
      lcd.setCursor(0,1);
      lcd.print("   On");
      lcd.setCursor(0,2);
      lcd.print("-> Off");
      break;
    case (98) :
      lcd.print("PID Parameters");
      lcd.setCursor(0,1);
      lcd.print("   Back");
      lcd.setCursor(0,2);
      lcd.print("-> Autotune");
      lcd.setCursor(0,3);
      lcd.print("   Manual");
      break;
      case (99) :
      lcd.print("Kp");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      break;
      case (100) :
      lcd.print("Ki");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      break;
      case (101) :
      lcd.print("Kd");
      lcd.setCursor(0,1);
      lcd.print(encoderPos);
      break;
      case (102) :
      lcd.print("PID Parameters");
      lcd.setCursor(0,1);
      lcd.print("-> Back");
      lcd.setCursor(0,2);
      lcd.print("   Autotune");
      lcd.setCursor(0,3);
      lcd.print("   Manual");
      break;
      
      case (105) :
        lcd.print("PID Parameters");
        lcd.setCursor(0,1);
        lcd.print("   Back");
        lcd.setCursor(0,2);
        lcd.print("   Autotune");
        lcd.setCursor(0,3);
        lcd.print("-> Manual");
      break;
      
      case (106) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("-> Kp");
        lcd.setCursor(0,2);
        lcd.print("   Ki");
        lcd.setCursor(0,3);
        lcd.print("   Kd");
      break;
      case (107) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("   Kp");
        lcd.setCursor(0,2);
        lcd.print("-> Ki");
        lcd.setCursor(0,3);
        lcd.print("   Kd");
      break;
      case (108) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("   Kp");
        lcd.setCursor(0,2);
        lcd.print("   Ki");
        lcd.setCursor(0,3);
        lcd.print("-> Kd");
      break;
      case (109) :
        lcd.print("   Kp");
        lcd.setCursor(0,1);
        lcd.print("   Ki");
        lcd.setCursor(0,2);
        lcd.print("   Kd");
        lcd.setCursor(0,3);
        lcd.print("-> On/Off");
      break;
      case (110) :
        lcd.print("-> Back");
        lcd.setCursor(0,1);
        lcd.print("   Kp");
        lcd.setCursor(0,2);
        lcd.print("   Ki");
        lcd.setCursor(0,3);
        lcd.print("   Kd");
      break;
      
      case (111):
        lcd.print("Kp");
        lcd.setCursor(0,1);
        lcd.print(encoderPos);  
      break;
      case (112):
       lcd.print("Ki");
        lcd.setCursor(0,1);
        lcd.print(encoderPos);
      break;
      case (113) :
        lcd.print("Kd");
        lcd.setCursor(0,1);
        lcd.print(encoderPos);
      break;
      case (114) :
        lcd.print("-> Back");
        lcd.setCursor(0,1);
        lcd.print("   On");
        lcd.setCursor(0,2);
        lcd.print("   Off");
      break;
      case (115) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("-> On");
        lcd.setCursor(0,2);
        lcd.print("   Off");
      break;
      case (116) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("   On");
        lcd.setCursor(0,2);
        lcd.print("-> Off");
      break;
      
      case (117) :
        lcd.print("   Ki");
        lcd.setCursor(0,1);
        lcd.print("   Kd");
        lcd.setCursor(0,2);
        lcd.print("   On/Off");
        lcd.setCursor(0,3);
        lcd.print("-> Autotune");
      break;
      
      case (118) :
        lcd.print("-> Back");
        lcd.setCursor(0,1);
        lcd.print("   On");
        lcd.setCursor(0,2);
        lcd.print("   Off");
      break;
      case (119) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("-> On");
        lcd.setCursor(0,2);
        lcd.print("   Off");
      break;
      case (120) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("   On");
        lcd.setCursor(0,2);
        lcd.print("-> Off");
      break;
      
      case (121) :
      lcd.print("Start auger, feed");
      lcd.setCursor(0,1);
      lcd.print("filament through");
      lcd.setCursor(0,2);
      lcd.print("puller and guide");
      lcd.setCursor(0,3);
      lcd.print("then select PIDStart");
      break;
      
      case (122):
      lcd.print("PID Start");
      lcd.setCursor(0,1);
      lcd.print("Once winder starts,");
      lcd.setCursor(0,2);
      lcd.print("diameter is steady");
      lcd.setCursor(0,3);
      lcd.print("then begin spooling");
      break;      
      
      case (123) :
      lcd.print("Once spooling ready");
      lcd.setCursor(0,1);
      lcd.print("click to begin guide");
      break;
      
      case (124):
      lcd.print("Waiting for");
      lcd.setCursor(0,1);
      lcd.print("steady state");
      lcd.setCursor(0,2);
      lcd.print(currentTemp);
      lcd.print("/");
      lcd.print(desiredTemp);
      lcd.print(" C");
      lcd.setCursor(0,3);
      lcd.print(filamentDiameter);
      lcd.print("/");
      lcd.print(desiredFilamentDiameter);
      break;
      
      case (125):
      lcd.print("Wait....");
      lcd.setCursor(0,1);
      lcd.print("Autotune in Process");
      lcd.setCursor(0,2);
      break;
      
      case (126) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("-> On");
        lcd.setCursor(0,2);
        lcd.print("   Off");
      break;
      case (127) :
        lcd.print("   Back");
        lcd.setCursor(0,1);
        lcd.print("   On");
        lcd.setCursor(0,2);
        lcd.print("-> Off");
      break;
      
}
  
    auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
   
}




//This is what is displayed on the info screen, 

void infoScreen(double currentTemp) {
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  
  if (mode == 1) { //possibly add motor run fuctions
    
    lcd.print("Auto ");
    lcd.print(diameterAverage2);
    lcd.print("/");
    lcd.print(desiredFilamentDiameter);
    lcd.print("mm");
    lcd.setCursor(0,1);

    lcd.print(currentTemp);
    lcd.print("/");
    lcd.print(desiredTemp);
    lcd.print(" C");

    lcd.setCursor(0,2);
    lcd.print("Control delay = ");
    lcd.print(diameterDelay);
    lcd.setCursor(0,3);
    lcd.print("Puller pulses = ");
    lcd.print(pullerPulses);
  /*
  if (autotuneVariable == 0) {
    lcd.print("Kp = ");
    lcd.print(Kp);
    lcd.print("; Ki = ");
    lcd.print(Ki);
    lcd.setCursor(0,3);
    lcd.print("Kd = ");
    lcd.print(Kd);
  }
  else {
    lcd.print("Autotune Enabled");
  }
  */
  
  
  }
  else {
    lcd.print("Manual ");
    lcd.print(diameterAverage2);
    lcd.print("/");
    lcd.print(desiredFilamentDiameter);
    lcd.print(" mm");
    lcd.setCursor(0,1);
    lcd.print(currentTemp);
    lcd.print("/");
    lcd.print(desiredTemp);
    lcd.print(" C");
  }
  
  
  
  
  directory = 0;
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();

}
