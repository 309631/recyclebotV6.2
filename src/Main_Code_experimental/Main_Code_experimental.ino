/*
Make sure that the AccelStepper library is installed to ensure functionality.


All menu functionality is based on the associated MenuDesign.txt file. The directory file relates to the menu that
the system is in. This is here so the when the encoder changes, the system knows where it is at.

The state variable is present to let the system know what to do when a button is pressed, as well as what to output to the screen.

Once either the button is pressed, or the encoder has been moved, the system updates the screen and makes the desired changes.


Note all thermistors will have different tuning functions, see the Thermistor function below to tune the steinhart coefficients to
the appropriate values (This is done by measuring resistances of the thermistor at different temperatures, then performing a 
regression to approximate values. This is done for the thermistors we speced out on the OSHE BOM, see appropedia Recyclebot V5.1)


Future improvements include

1) Increased cooling capacity
2) Better control algrothims for both barrel temperature and automatic filament diameter modes.
3) Code could be much more condensed and efficient. Re-do to make streemlined, easier to understand, and troubleshoot
4) Storage of variables within code for plastic types
5) SD Card output of diameter as a function of length or time
6) Better spooling function to ensure the entire role is of printable quality
7) More sensitive motor speed adjustments
8) Change all coded labels of RPM to actually output RPM on the machine
9) All modes only tested on 1.75mm filiment diameter settings. 
10) Lookup tables for thermistors, which can be stored and easily changed as opposed to the steinhart approximations.


For an overview of encoder code, see:
http://www.instructables.com/id/Easy-Arduino-Menus-for-Rotary-Encoders/step2/Code/


*/
 
#include <math.h>
#include "AccelStepper.h"
#include "config.h"




//_______________________________________________________________________________________
//DEFINES
//_______________________________________________________________________________________


//LCD SETUP___________________________________________________
#if LCD_I2c == 1
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4); // I2C address 0x27, 20 column and 4 rows

#else
#include <LiquidCrystal.h>
LiquidCrystal lcd(32,47,45,43,41,39,37);

#endif

//_______________________________________________________________________________________

int mode = 0; //auto = 1, manual = 0

//______________________________________________
//dictates screen state
int state = 79;
int directory = 31;


// for diameter reading modes
double oldDiameterReading;
unsigned long oldDiameterReadingTime = millis();


int pullerReadyVariable = 0; //For on/off purposes of puller

int startSequence = 1;  // For filament guide to home at power on, turns to 0 after limit switch is pressed

//______________________________________________________________________
//all variables to resume current settings at setting screens when wish to change 
int execution = 1;
int initialSetting = 0;
int variable = 0;
int variable1 = 0;
int variable2 = 0;
int variable3 = 0;
int guideVariable = 0;
//__________________________________________________________________________________________
double upperFilamentBound;
double lowerFilamentBound;


unsigned int diameterDelay = 25000;
unsigned long lastAdjustmentTime = millis();

unsigned long lastInputTime = 0;
int Y = 0; //For acceleration purposes


//_______________________________________________________________________________________
//Motor speed controls

/* int reductionValue = 18; */
int desiredAugerRPM = (35*REDUCTION)/15; //calculating
int currentAugerRPM = 0;

int pullerRPM = 70;
double pullerPulses;
double pullerPulsesAuto;

int winderRPM = 2;
int winderPulses;
//______________________________________________________________________________________
//filament diameter
double desiredFilamentDiameter;
double filamentDiameter;
int filamentDiameterPin = A3;

//_______________________________________________________________________________________
//X pins for auger motor output

int augerEnable = 38;
int augerpulses;
int augerStep = A0;  
int augerDIR = A1;


//______________________________________________________________________________________
//puller control pins Y pins motor output
int pullerEnable = A2;


int pullerStep = A6;
int pullerDIR = A7;
//______________________________________________________________________________________
//winder control pins E0 pins motor output
int winderEnable = 24;

int winderStep = 26;
int winderDIR = 28;

//___________________________________________________________________________
//z pins
int guideEnable = A8;

int guideStep = 46;
int guideDIR = 48;

int guideStart;
int guideStop;
int guidePos;

int filamentDirection = 1;

//_____________________________________________________________________________________
//Temperature control - relay outut
int heaterRelayPin = 1;

//OSHE Recyclebot V5.1 as on appropedia uses A13.
int thermistorPin = A15;

int heaterEnable = 0;
double desiredTemp = 0;
double currentTemp;
bool heaterState = LOW;
bool oldHeaterState = LOW;

int heaterVariable = 0;

double val;                
double temp;

//___________________________________________________________________________________________
//Temperature smoothing function
const int numTempReadings = 20;

double tempReadings[numTempReadings];
int tempIndex = 0;
double tempTotal = 0;
double tempAverage = 0;
//_____________________________________________________________________________________________
//Diameter smoothing function
const int numdiameter1Readings = 20;
const int numdiameter2Readings = 20;

double diameter1Readings[numTempReadings];
double diameter2Readings[numTempReadings];

int diameterIndex1 = 0;
int diameterIndex2 = 0;

double diameter1Total = 0;
double diameter2Total = 0;

double diameterAverage1 = 0;
double diameterAverage2 = 0;

//_______________________________________________________________________________________
//stepper definitions
AccelStepper auger(1, augerStep, augerDIR);
AccelStepper puller(1, pullerStep, pullerDIR);
AccelStepper winder(1, winderStep, winderDIR);
AccelStepper guide(1, guideStep, guideDIR);
//___________________________________________________________________________________
//ecoder setup

static int pinA = 2;   //interrupt pins for scrollable navigation
static int pinB = 3;
volatile byte aFlag = 0;
volatile byte bFlag = 0;

volatile byte encoderPos = 0;
volatile byte oldEncoderPos = 0;
volatile byte reading = 0;

const byte buttonPin = 14;      // button for selection
byte oldButtonState = HIGH;
const unsigned long debounceTime = 100; //milliseconds; to eliminate switch bounce
unsigned long buttonPressTime; //when the switch last changed state
boolean buttonPressed = 0;

//__________________________________________________________________________________________
//Limit Switch setup

const byte buttonPin1 = 11; //filament guide end switch
byte oldButtonState1 = LOW;
unsigned long buttonPressTime1; //when the switch last changed state
boolean buttonPressed1 = 0;

 const byte buttonPin2 = 6;   //Winder revolution tracker end switch
byte oldButtonState2 = LOW;
unsigned long buttonPressTime2;
boolean buttonPressed2 = 0;

//__________________________________________________________________________________________
// coolingsetup
// fan stands for the 0-12 V output 
bool fanState = true;
bool oldFanState = true;
double fanSpeed = 0;//0-1023 analog output

int fanPin = 10;


//___________________________________________________________________________________________
//______________________________________________________________________________________________
//______________________________________________________________________________________________

void setup() {
  
  //Serial.begin(9600); //troubleshooting
  //pin declorations
  pinMode(fanPin, OUTPUT);
  pinMode(heaterRelayPin, OUTPUT);
  pinMode(filamentDiameterPin, INPUT);
  
  filamentDiameter = analogRead(filamentDiameterPin);
  filamentDiameter = (filamentDiameter*5)/1023; // convert to a 0-5V scale (Voltage correlates to filament diameter)
  
  
  
  
  //______________________________________________________________________________
 //questionable sectin
  auger.setMaxSpeed(50000);
  auger.setAcceleration(500);

  puller.setMaxSpeed(50000);
  puller.setAcceleration(10000);

  winder.setMaxSpeed(500000);
  winder.setAcceleration(100000);
  
  guide.setMaxSpeed(500000);
  guide.setAcceleration(200000);
//_________________________________________________________________________________________
//auger set up
  pinMode(augerEnable, OUTPUT);
  pinMode(augerStep, OUTPUT);
  pinMode(augerDIR, OUTPUT);

  digitalWrite(augerEnable, HIGH);
  digitalWrite(augerStep, LOW);
  digitalWrite(augerDIR, HIGH);

//_________________________________________________________________________________________
//puller set up
  pinMode(pullerEnable, OUTPUT);
  pinMode(pullerStep, OUTPUT);
  pinMode(pullerDIR, OUTPUT);

  digitalWrite(pullerEnable, HIGH);
  digitalWrite(pullerStep, LOW);
  digitalWrite(pullerDIR, HIGH);
//____________________________________________________________________________
//Winder set up
  pinMode(winderEnable, OUTPUT);
  pinMode(winderStep, OUTPUT);
  pinMode(winderDIR, OUTPUT);

  digitalWrite(winderEnable, HIGH);
  digitalWrite(winderStep, LOW);
  digitalWrite(winderDIR, LOW);

//______________________________________________________________________________
//guide setup
  pinMode(guideEnable, OUTPUT);
  pinMode(guideStep, OUTPUT);
  pinMode(guideDIR, OUTPUT);

  digitalWrite(guideEnable, LOW);
  digitalWrite(guideStep, LOW);
  digitalWrite(guideDIR, HIGH);
  
//______________________________________________________________________________
//Set initial values
  augerpulses = 200*desiredAugerRPM;
  auger.setSpeed(augerpulses);
  currentAugerRPM = desiredAugerRPM;
  
  pullerPulses = pullerRPM*100;
  puller.setSpeed(pullerPulses);
  winderPulses = winderRPM*20;
  winder.setSpeed(winderPulses);
  
  guide.setSpeed(-2500);

//________________________________________________________________________________
//User interface start

	//LCD SETUP___________________________________________________
#if LCD_I2c == 1
	lcd.init();
	lcd.backlight();
	lcd.clear();
#else
  lcd.begin(20,4);
	lcd.clear();
#endif



  screenUpdate(state,currentTemp);

  pinMode(pinA, INPUT_PULLUP);
  pinMode(pinB, INPUT_PULLUP);
  attachInterrupt(0, PinA, RISING);
  attachInterrupt(1, PinB, RISING);
  
//user interface end

//Setup ends

//_________________________________________________________________________________
  
//Limit Switch Code
  pinMode(buttonPin, INPUT_PULLUP);   //user interface button
  pinMode(buttonPin1, INPUT_PULLUP); //guide button
  pinMode(buttonPin2, INPUT_PULLUP); //Winder button

  //running average functions to reduce measurement noise

 for(int thisTempReading = 0; thisTempReading < numTempReadings; thisTempReading++) {
   tempReadings[thisTempReading] = 0;
 }
 for(int thisDiameterReading = 0; thisDiameterReading < numdiameter1Readings; thisDiameterReading++) {
   diameter1Readings[thisDiameterReading] = 0;
 }
 
 for(int thisDiameterReading = 0; thisDiameterReading < numdiameter2Readings; thisDiameterReading++) {
    diameter2Readings[thisDiameterReading] = 0; 
 }

 
}
//________________________________________________________________________________


//must call .runspeed() function often to continue to sustain motor speed. The more the function is called, the faster
//the stepper motors can go.
void loop() {
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
  while (startSequence == 1) {  // This is the block that is only ran once while the filament guide is homed.
     guide.runSpeed(); //run set speed until the switch is pressed
    
     byte buttonState1 = digitalRead(buttonPin1);
      if (buttonState1 != oldButtonState1){
        if (millis () - buttonPressTime1 >= debounceTime){
          buttonPressTime1 = millis ();
          oldButtonState1 = buttonState1;
        if (buttonState1 == LOW){
          buttonPressed1 = 1;
        }
        else {
  
            buttonPressed1 = 0;
        }
      }
    }
    
guide.runSpeed();
    
    if (buttonPressed1 == 1) {
      buttonPressed1 = 0;
      startSequence = 2;
      guidePos = 0;
      guide.setCurrentPosition(guidePos);
    
    guidePos = 248;
    guide.moveTo(guidePos); 
  guide.runSpeedToPosition();
    
   
  }
  }
  
  
if (guidePos >= guideStop) {
    filamentDirection = 0;
  }
  else if(guidePos <= guideStart) {
    filamentDirection = 1;
  }
  
  
    auger.runSpeed();
    puller.runSpeed();
    winder.runSpeed();
    guide.runSpeedToPosition();



//initial timeout until motor has been set  
  
  rotaryMenu();

  val = analogRead(thermistorPin);
  currentTemp = Thermistor(val);

  
  filamentDiameter = analogRead(filamentDiameterPin);
  filamentDiameter = (filamentDiameter *  5)/1023;
  
  diameter1Total = diameter1Total - diameter1Readings[diameterIndex1];
  diameter1Readings[diameterIndex1] = filamentDiameter;
  
  diameter1Total = diameter1Total + diameter1Readings[diameterIndex1];
  diameterIndex1 = diameterIndex1 + 1;
  
  
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
  if (diameterIndex1 >= numdiameter1Readings){
    diameterIndex1 = 0;
  }
  
  diameterAverage1 = diameter1Total/numdiameter1Readings;
  
  diameter2Total = diameter2Total - diameter2Readings[diameterIndex2];
  diameter2Readings[diameterIndex2] = diameterAverage1;
  
  diameter2Total = diameter2Total + diameter2Readings[diameterIndex2];
  diameterIndex2 = diameterIndex2 + 1;
  
  if (diameterIndex2 >= numdiameter2Readings){
    diameterIndex2 = 0;
  }
  
  diameterAverage2 = diameter2Total/numdiameter2Readings;


  //___________________________________________________________________________________________________________
  //Auto mode code
  if ((mode == 1) && ((millis () - lastAdjustmentTime) >= diameterDelay)) { 
      if (diameterAverage2 > (upperFilamentBound+1)) {
        pullerPulsesAuto = pullerPulses + 1000;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses += 200;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 < (lowerFilamentBound-1)) {
        pullerPulsesAuto = pullerPulses - 1000;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses -= 200;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 > (upperFilamentBound+0.5)) {
        pullerPulsesAuto = pullerPulses + 400;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses += 40;
        winder.setSpeed(winderPulses);
        
      }
      else if (diameterAverage2 < (lowerFilamentBound-0.5)) {
        pullerPulsesAuto = pullerPulses - 400;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses -= 40;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 > (upperFilamentBound+0.25)) {
        pullerPulsesAuto = pullerPulses + 200;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses += 20;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 < (lowerFilamentBound-0.25)) {
        pullerPulsesAuto = pullerPulses - 200;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses -= 20;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 > (upperFilamentBound + 0.125)) {
        pullerPulsesAuto = pullerPulses + 20;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses += 4;
        winder.setSpeed(winderPulses);

      }
      else if (diameterAverage2 < (lowerFilamentBound - 0.125)) {
        pullerPulsesAuto = pullerPulses - 20;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses -= 4;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 > (upperFilamentBound)) {
        pullerPulsesAuto = pullerPulses + 2;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses += 4;
        winder.setSpeed(winderPulses);
      }
      else if (diameterAverage2 < (lowerFilamentBound)) {
        pullerPulsesAuto = pullerPulses - 2;
        pullerPulses = pullerPulsesAuto;
        puller.setSpeed(pullerPulsesAuto);
        winderPulses -= 4;
        winder.setSpeed(winderPulses);
      }
      lastAdjustmentTime = millis ();
      diameterDelay = (2000000000/pullerPulses);
  }
  
  
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
    
  //guide.runToPosition();
  //diameterMonitor
  
  //if switchpressed, increment by diameter width
  
//menu screena timer
//  if ((directory != 0) && (millis () - lastInputTime >= 5000)){
//    infoScreen();
//    directory = 0;
//    encoderPos = 0;
//   }


  if ((directory == 0) && ((millis() - lastInputTime) >= 1000)) { //screen update to show current information
    screenUpdate(0, currentTemp);    
    auger.runSpeed();
    puller.runSpeed();
    winder.runSpeed();
    guide.runSpeedToPosition();
  }
  
  temperatureControl(heaterEnable, desiredTemp, heaterState, currentTemp); //function to control heater output
  


} 


//________________________________________________________________________________
//Rotary encoder interrupt service routine for one encoder pins
void PinA(){
  cli();
  reading = PINE & 0x30;
  if (reading == B00110000 && aFlag){
    encoderPos --;
    bFlag = 0;
    aFlag = 0;
  }
  else if (reading == B00010000) bFlag = 1;
  sei();
}

void PinB(){
  cli();
  reading = PINE & 0x30;
  if (reading == B00110000 && bFlag){
    encoderPos ++;
    bFlag = 0;
    aFlag = 0;
  }
  else if (reading == B00100000) aFlag = 1;
  sei();
}


//__________________________________________________________________________________________________________________
//Functions menu below
//__________________________________________________________________________________________________________________
double Thermistor(int RawADC) {
  double temp;
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
 
 double vRead;
 double extra;
  vRead = (RawADC*5);
  vRead = vRead/1024;
  
 temp = 4700*vRead;
 extra = 5-vRead;
 temp = temp/extra;
 
 temp = log(temp);
 //steinhart coefficients
 //_____________A_______________________B__________________C
 temp = 1 / (0.0018048682531586 + (-.00002394158923334 + (.0000011957897892998 * temp * temp ))* temp );
 
 temp = temp - 273.15;          
  
 
  //subtract the last reading
  tempTotal = tempTotal - tempReadings[tempIndex];
  tempReadings[tempIndex] = temp;
  tempTotal = tempTotal + tempReadings[tempIndex];
  tempIndex = tempIndex + 1;
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
  if (tempIndex >= numTempReadings){
    tempIndex = 0;
  }
  tempAverage = tempTotal/numTempReadings;
  

 
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
   
 return tempAverage;
}
//_________________________________________________________________________________________________  


void temperatureControl(int heaterEnable, int desiredTemp, bool heaterState, double currentTemp) {
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
  if ((heaterEnable == 1) && (currentTemp < (desiredTemp))){
    //currentTemp = augerTemp.readCelsius();
    
      heaterState = HIGH;
 }
   else {
      heaterState = LOW;
      if (heaterVariable == 1) {
        heaterVariable = 2;
      }
      else if (heaterVariable = 2){
        heaterVariable = 3;
      }
      
    }
    if (heaterState != oldHeaterState) {
       
     auger.runSpeed();
     puller.runSpeed();
     winder.runSpeed();
      guide.runSpeedToPosition();
      
      if (heaterState == HIGH) {
        digitalWrite(heaterRelayPin, HIGH);
      }
      else {
        digitalWrite(heaterRelayPin, LOW);
      }
      oldHeaterState = heaterState;
    }
    
}
  
void fanControl(int fanSpeed) {
  if (fanState != oldFanState) { 
    analogWrite(fanPin, fanSpeed);
    oldFanState = fanState;
  }
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
}

  
 
  //_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
  //LCD and selector Settings
  //Menu navigation. See MenuDesign.txt to understand correlation of variables.
void rotaryMenu(){
  
  
  if(oldEncoderPos != encoderPos){
//__________________________
// Menu Navigation
      // encoder position within a menu screen, used for navigation
      if (directory == 0){
        directory = 1;
        switch (encoderPos) {
          case (1) :
            state = 1;
            directory = 1;
            break;
            case (2) :
                state = 2;
                break;
              case (3) :
                state = 49;
                break;  
              case (4) :
                state = 1;
                encoderPos = 1;
                break;
              case (0) :
                encoderPos = 3;
                state = 49;
                break;
        }
        
      }
      if (directory == 1) {
        switch (encoderPos) {
          case (1) :
            state = 1;
            break;
            case (2) :
                state = 2;
                break;
              case (3) :
                state = 49;
                break;  
              case (4) :
                state = 1;
                encoderPos = 1;
                break;
              case (0) :
                encoderPos = 3;
                state = 49;
                break;
        }
      }
         
         else if (directory == 2) {
           switch (encoderPos) {
             case (0) :
               state = 50;
               break;
             case (1) :
              state = 51;
              break;
            
            case (2) :
              state = 57;
              break;
            
            case (3) :
              state = 58;
              break;
            
            case (4) :
              state = 50;
              encoderPos = 0;
              break;
            
            case (255) :
              state = 58;
              encoderPos = 3;
              break;
            
           }
         }
         
        else if (directory == 3) {
          switch(encoderPos) {
            case (1) : //
              state = 3;
              break;
            
            case (2) :
              state = 4;
              break;
            
            case (3) :
              state = 14; //servo
              break;
            
            case (4) :
              state = 22;
              break;
            
            case (5) : // auger
              state = 28;
              break;
            
            case (6) : //winder
              state = 35;
              break;
            
            case (7) : // Heater
              state = 42;
              break;
            
            case (8) :
              state = 3;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 42;
              encoderPos = 7;
              break;
            
          }
        }    
        
        else if (directory == 4) {
          switch (encoderPos) {
            case (1) :
              state = 62;
              break;
            
            case (2) :
              state = 9;
              break;
            
            case (3) :
              state = 62;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 9;
              encoderPos = 2;
              break;
          
        }
        }
        else if (directory == 5) {
          switch (encoderPos) {
            case (1) :
              state = 63;
              break;
            
            case (2) :
              state = 6;
              break;
            
            case (3) :
              state = 9;
              break;
            
            case (4) :
              state = 63;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 9;
              encoderPos = 3;
              break;
            
          }
        }
 
        
        else if (directory == 8) {
          switch (encoderPos) {
            case (1) :
              state = 15;
              break;
            
            case (2) :
              state = 126;
              break;
            
            case (3) :
              state = 127;
              break;
                       
            case (4) :
              state = 15;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 127;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 9) {
          switch (encoderPos) {
            case (1) :
              state = 17;
              break;
            
            case (2) :
              state = 18;
              break;
            
            case (3) :
              state = 19;
              break;
            
            case (4) :
              state = 17;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 19;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 10) {
          switch (encoderPos) {
            case (1) :
              state = 23;
              break;
            
            case (2) :
              state = 24;
              break;
            
            case (3) :
              state = 77;
              break;
            case (4):
            state = 23;
            encoderPos = 1;
            break;
            
            case (0) :
              state = 77;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 11) {
          switch (encoderPos) {
            case (1) :
              state = 25;
              break;
            
            case (2) :
              state = 26;
              break;
            
            case (3) :
              state = 27;
              break;
            
            case (4) :
              state = 25;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 27;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 12) {
          switch (encoderPos){
            case (1) :
              state = 29;
              break;
            
            case (2) :
              state = 30;
              break;
            
            case (3) :
              state = 34;
              break;
            
            case (4) :
              state = 29;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 34;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 13) {
          switch (encoderPos) {
            case (1) :
              state = 31;
              break;
            
            case (2) :
              state = 32;
              break;
            
            case (3) :
              state = 33;
              break;
            
            case (4) :
              state = 31;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 33;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 14) {
          switch (encoderPos) {
            case (1) :
              state = 36;
              break;
            
            case (2) :
              state = 37;
              break;
            
            case (3) :
              state = 41;
              break;
            
            case (4) :
              state = 36;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 41;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 15) {
          switch (encoderPos) {
            case (1) :
              state = 38;
              break;
            
            case (2) :
              state = 39;
              break;
            
            case (3) :
              state = 40;
              break;
            
            case (4) :
              state = 38;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 40;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 16) {
          switch (encoderPos) {
            case (1) :
              state = 43;
              break;
            
            case (2) :
              state = 44;
              break;
            
            case (3) :
              state = 48;
              break;
            
            case (4) :
              state = 43;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 48;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 17) {
          switch (encoderPos) {
            case (1) :
              state = 45;
              break;
            
            case (2) :
              state = 46;
              break;
            
            case (3) :
              state = 47;
              break;
            
            case (4) :
              state = 45;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 47;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 18) {
          switch (encoderPos) {
            case (1) :
              state = 50;
              break;
            
            case (2) :
              state = 84;
              break;
            
            case (3) :
              state = 85;
              break;
            
            case (4) :
              state = 50;
              encoderPos = 1;
              break;
            
            
            case (0) :
              state = 85;
              encoderPos = 3;
              break;
            
          }
        }
        
        else if (directory == 19) {
          switch (encoderPos) {
            case (1) :
              state = 52;
              break;
            
            case (2) :
              state = 53;
              break;
            
            case (3) :
              state = 54;
              break;
            
            case (4) :
              state = 55;
              break;
            
            case (5):
              state = 56;
              break;
            
            case (6) :
              state = 52;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 56;
              encoderPos = 5;
              break;
            
          }
        }
        
        else if (directory == 20){
          switch (encoderPos) {
            case (1) :
              state = 59;
              break;
            
            case (2) :
              state = 60;
              break;
            
            case (3) :
              state = 61;
              break;
            
            case (4) :
              state = 59;
              encoderPos = 1;
              break;
            
            case (0) :
              state = 61;
              encoderPos = 3;
              break;
          }
          }
          else if (directory == 21) {
            switch (encoderPos) {
              case (1) :
              state = 71;
              break;
              
              case (2) :
                state = 72;
                break;
              
              case (3) :
                state = 73;
                break;
              
              case (4) :
                state = 71;
                encoderPos = 1;
                break;
              
              case (0) :
                state = 73;
                encoderPos = 3;
                break;
              
            }
          }
          else if (directory == 22) { // state = 67
            if (variable3 == 0) {
              encoderPos = fanSpeed/2.55;
              variable3 = 1;
            }
            
            if (encoderPos == 255) {
              encoderPos = 100;
            }
            else if (encoderPos == 101) {
              encoderPos = 0;
            }
          }
          /*
          else if (directory == 23) {//state = 68
            if ((encoderPos < Xdeg) || (encoderPos > Ydeg)) {
              encoderPos = oldEncoderPos;
            }
          }
          
          else if (directory == 24) { //state = 69
            if ((encoderPos < Xdeg) || (encoderPos > Ydeg)) {
              encoderPos = oldEncoderPos;
            }
          }
          
          else if (directory == 25) { //state = 70
            if ((encoderPos < Xdeg) || (encoderPos > Ydeg)) {
              encoderPos = oldEncoderPos;
            }
          }
          */
          else if (directory == 27) { //state = 74
            if (variable2 == 0) {
              encoderPos = desiredAugerRPM;
              currentAugerRPM = desiredAugerRPM;
              variable2 = 1;
            }
            if ((encoderPos == 255) || (encoderPos == 100)) {
              encoderPos = oldEncoderPos;
            }
          }
          
          else if (directory == 26) {// state = 78
            if (variable == 0) {
              encoderPos = pullerRPM;
              variable = 1;
            }
            if ((encoderPos == 255) || (encoderPos == 71)) {
              encoderPos = oldEncoderPos;
            }
          }
          
          else if (directory == 28) {
            if (variable1 == 0) {
              encoderPos = winderRPM;
              variable1 = 1;
            }
            
            if (encoderPos == 255) {
              encoderPos = 0;
            }
            else if (encoderPos == 40) {
              encoderPos = 40;
            }
          }
          else if (directory == 29) {
            if (initialSetting == 0) {
              encoderPos = desiredTemp;
              initialSetting = 1;  
            }
          }
          

          
          else if (directory == 31) {
            state = 80;
            directory = 32;
          }
          else if (directory == 32) {
            if (encoderPos == 0) {
              encoderPos = 1;
             }  
            else if (encoderPos == 49) {
              encoderPos = 48;
             }
            
            guidePos = encoderPos*248;
            guide.moveTo(guidePos);
            guide.runSpeedToPosition();
          }
          else if (directory == 33) {
            if (execution == 1) {
               encoderPos = guideStart / 248;
               execution = 2;
             }
             if (encoderPos == 0) {
              encoderPos = 1;
             }  
            else if (encoderPos == 49) {
              encoderPos = 48;
             }
             
            guidePos = encoderPos*248;
            guide.moveTo(guidePos);
            guide.runSpeedToPosition();
          }
          else if (directory == 34) {
            switch (encoderPos) {
              case 1:
                state = 82;
                break;
              case 2:
                state = 83;
                break;
              case 3:
                state = 82;
                encoderPos = 1;
                break;
              case 0:
                state = 83;
                encoderPos = 2;
                break;
            }
          }
          else if (directory == 35) {
            switch (encoderPos) {
              case 1:
                state = 86;
                break;
              case 2:
                state = 87;
                break;
              case 3:
                state = 88;
                break;
              case 4:
                state = 89;
                break;
              case 5:
                state = 90;
                break;
              case 6:
                state = 86;
                encoderPos = 1;
                break;
              case 0:
                state = 90;
                encoderPos = 5;
                break;
            }
          }


          else if (directory == 36) {
            
            if (initialSetting == 0) {
              encoderPos = desiredTemp;
              initialSetting = 1;  
            }
          
          }
      else if (directory == 37) {
            if (variable2 == 0) {
              encoderPos = desiredAugerRPM;
              currentAugerRPM = desiredAugerRPM;
              variable2 = 1;
            }
            if ((encoderPos == 255) || (encoderPos == 100)) {
              encoderPos = oldEncoderPos;
            }
          }
      else if (directory == 38) {
            if (variable == 0) {
              encoderPos = pullerRPM;
              variable = 1;
            }
            if ((encoderPos == 255) || (encoderPos == 100)) {
              encoderPos = oldEncoderPos;
            }
            }
          
      else if (directory == 39) {
            if (variable1 == 0) {
              encoderPos = winderRPM;
              variable1 = 1;
            }
            
            if (encoderPos == 255) {
              encoderPos = 0;
            }
            else if (encoderPos == 40) {
              encoderPos = 40;
            }
          }
      else if (directory == 40) {
            if (variable3 == 0) {
              encoderPos = fanSpeed/2.55;
              variable3 = 1;
            }
            
            if (encoderPos == 255) {
              encoderPos = 100;
            }
            else if (encoderPos == 101) {
              encoderPos = 0;
            }
          }
      
      else if (directory == 41) {
          switch (encoderPos) {
              case 1:
                state = 96;
                break;
              case 2:
                state = 97;
                break;
              case 3:
                state = 96;
                encoderPos = 1;
                break;
              case 0:
                state = 97;
                encoderPos = 0;
                break;
              }
      }
      
      
      else if (directory == 51) {
        switch (encoderPos) {
              case 1:
                state = 114;
                break;
              case 2:
                state = 115;
                break;
              case 3:
                state = 116;
                break;
              case 4:
                state = 114;
                encoderPos = 1;
                break;
               case 0:
                   state = 116;
                   encoderPos = 3;
                 break;
              }
      }
      
       
        
        
        auger.runSpeed();
        puller.runSpeed();
        winder.runSpeed();
        guide.runSpeedToPosition();
        
        
        
        screenUpdate(state, currentTemp);
//____________________________   
    
        oldEncoderPos = encoderPos;
  }

  byte buttonState = digitalRead(buttonPin); //user interface button
  if (buttonState != oldButtonState){ //button pressed or released
    if (millis () - buttonPressTime >= debounceTime){
      buttonPressTime = millis ();
      oldButtonState = buttonState;
      if (buttonState == LOW){
        buttonPressed = 1;
      }
      else {
  
        buttonPressed = 0;
      }
    }
  }
  //____________________________________________________________________________________________________
  if (guideVariable == 1) {
  
  if ((directory != 31) && (directory != 32) && (directory != 33) && (directory != 34)) {
  byte buttonState2 = digitalRead(buttonPin2);
      if (buttonState2 != oldButtonState2){
        if (millis () - buttonPressTime2 >= debounceTime){
          buttonPressTime2 = millis ();
          oldButtonState2 = buttonState2;
        if (buttonState2 == LOW){
          buttonPressed2 = 1;
        }
        else {
  
            buttonPressed2 = 0;
        }
      }
    }

    if (buttonPressed2 == 1) {
      buttonPressed2 = 0;
      
      if (filamentDirection == 1) {
        guidePos = guidePos + 227 ;
      }
      else {
        guidePos = guidePos - 227;        
      }
      guide.moveTo(guidePos);
    }
    
    auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
  }
  }
  
  auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();

 //Navigation
 //_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
 //if button pressed, perform action
 if (buttonPressed == 1) {
   buttonPressed = 0;
    switch (state) {
      case (0) :
        state = 1;
        directory = 1;
        break;
      
      case (1) :
        state = 0;
        directory = 0;
        break;
      
      case (2) :
        state = 3;
        directory = 3;
        break;
      
      case (3) :
        state = 1;
        directory = 1;
        break;
      
      case (4) :
        state = 62;
        directory = 4;
        break;
   
      case (9):
        directory = 22;
        state = 67;
        break;
      
      case (14) :
        directory = 8;
        state = 15;
        break;
      
      case (15) :
        directory = 3;
        state = 3;
        break;
      
      case (16) :
        directory = 9;
        state = 17;
        break;
      
      case (17) :
        state = 15;
        directory = 8;
        break;
      
      case (18) :
        directory = 23;
        state = 68;
        break;
      
      case (19) :
        directory = 24;
        state = 69;
        break;
      
      case (20) :
        directory = 25;
        state = 70;
        break;
      
      case (21) :
        directory = 21;
        state = 71;
        break;
      
      case (22) :
        directory = 10;
        state = 23;
        break;
      
      case (23) :
        directory = 3;
        state = 3;
        break;
      
      case (24) :
        directory = 11;
        state = 25;
        break;
      
      case (25) :
        directory = 10;
        state = 23;
        break;
      
      case (26) :
        digitalWrite(pullerEnable, LOW);
        directory = 10;
        state = 23;
        break;
      
      case (27) :
        digitalWrite(pullerEnable, HIGH);
        state = 23;
        directory = 10;
        break;
      
      
      case (28) :
        //if (variable1 == 0) {
        //  encoderPos = winderRPM;
        //}
        state = 29;
        directory = 12;
        break;
      
      case (29) :
        state = 3;
        directory = 3;
        break;
      
      case (30) :
        state = 31;
        directory = 13;
        break;
      
      case (31) :
        state = 29;
        directory = 12;
        break;
      
      case (32) :
        digitalWrite(augerEnable, LOW);
        state = 29;
        directory = 12;
        break;
      
      case (33) :
        digitalWrite(augerEnable, HIGH);
        state = 29;
        directory = 12;
        break;
      
      case (34) :
        state = 74;
        directory = 27;
        break;
      
      case (35) :
        state = 36;
        directory = 14;
        break;
      
      case (36) :
        directory = 3;
        state = 3;
        break;
      
      case (37) :
        state = 38;
        directory = 15;
        break;
      
      case (38) :
        state = 36;
        directory = 14;
        break;
      
      case (39) :
        digitalWrite(winderEnable, LOW);
        state = 36;
        directory = 14;
        break;
      
      case (40) :
        digitalWrite(winderEnable, HIGH);
        state = 36;
        directory = 14;
        break;
      
      case (41) :
        state = 75;
        directory = 28;
        break;
      
      case (42) :
        state = 43;
        directory = 16;
        break;
      
      case (43) :
        directory = 3;
        state = 3;
        break;
      
      case (44) :
        directory = 17;
        state = 45;
        break;
      
      case (45) :
        state = 43;
        directory = 16;
        break;
      
      case (46) :
        heaterEnable = 1;
        state = 43;
        directory = 16;
        break;
      
      case (47) :
        heaterEnable =0;
        state = 43;
        directory = 16;
        break;
      
      case (48) :
        state = 76;
        directory = 29;
        break;
      
      case (49) :
          state = 114;
          directory = 51;
        break;
      
      case (50) :
        state = 49;
        directory = 2;
        break;
      case (62) :
      state = 3;
      directory = 3;
      break;

        
     
      
      
      case (67) :
        variable3 = 0;
        fanSpeed = encoderPos*.01*255;
        state = 62;
        directory = 4;
        fanState = !oldFanState;
        fanControl(fanSpeed);
        break;
      
      case (68) :
        guideStart = encoderPos;
        state = 15;
        directory = 8;
        break;
      
      case (69) :
        guideStop = encoderPos;
        state = 15;
        directory = 8;
        break;
      
      case (70) :
        guidePos = encoderPos;
        //filamentGuide.write(guidePos);
        state = 15;
        directory = 3;
        break;
      
      case (74) :
        variable2 = 0;
        desiredAugerRPM = encoderPos;
        augerpulses = 200*desiredAugerRPM;
        auger.setSpeed(augerpulses);
        currentAugerRPM = desiredAugerRPM;
        state = 29;
        directory = 12;
        break;
      
      case (75) :
        variable1 = 0;
        winderRPM = encoderPos;
        winderPulses = 20*winderRPM;
        winder.setSpeed(winderPulses);
        state = 36;
        directory = 14;
        break;
      
      case (76) :
        desiredTemp = encoderPos;
        state = 43;
        directory = 16;
        initialSetting = 0;
        break;
      
      case (77) :
        state = 78;
        directory = 26;
        break;
      
      case (78) :
        variable = 0;
        pullerRPM = encoderPos;
        pullerPulses = pullerRPM*100;
        puller.setSpeed(pullerPulses);
        //winder.setSpeed(pullerPulses/10-100);
        state = 23;
        directory = 10;
        break;
        
      case (79) :
        state = 80;
        directory = 32;
        break;
     case (80) :
       
        state = 81;
        directory = 33;
        guideStart = encoderPos*248;
        break;
     case (81) :    //double check 
       

        guideStop = encoderPos*248;
        guidePos = guideStart;
        guide.moveTo(guidePos);

        //state = 86;       //state 82
        //directory = 35;
        desiredFilamentDiameter = 1.75;
        upperFilamentBound = 1.8;
        lowerFilamentBound = 1.7;



       state = 91;      //state 86
       directory = 36;
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       oldEncoderPos = 0;
       encoderPos = desiredTemp;
       
        
        break;
     case (82) :
        state = 86;
        directory = 35;
        desiredFilamentDiameter = 1.75;
        upperFilamentBound = 1.8;
        lowerFilamentBound = 1.7;
        break;
     case (83) :
        state = 86;
        directory = 35;
        desiredFilamentDiameter = 3.00;
        upperFilamentBound = 3.05;
        lowerFilamentBound = 2.95;
        break;
  
     case (86) :
       state = 91;
       directory = 36;
       
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       oldEncoderPos = 0;
       encoderPos = desiredTemp;
       
       break;
     case (87) :
       state = 91;
       directory = 36;

       
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       encoderPos = desiredTemp;
       
       break;
     case (88) :
       state = 91;
       directory = 36;
       
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       encoderPos = desiredTemp;

       
       break;
     case (89) :
       state = 91;
       directory = 36;
       
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       
       encoderPos = desiredTemp;

       
       break;
       
     case (90) :
       state = 91;
       directory = 36;
       
       desiredTemp = 150;
       desiredAugerRPM = 6;
       pullerRPM = 50;
       winderRPM = 80;
       fanSpeed = 128;
       encoderPos = desiredTemp;
   
       break;
    
      case (91) :
        desiredTemp = encoderPos;
        state = 92;
        directory = 37;
        initialSetting = 0;
        break;
      
      case (92) :
        variable2 = 0;
        desiredAugerRPM = encoderPos;
        augerpulses = 200*desiredAugerRPM;
        auger.setSpeed(augerpulses);
        currentAugerRPM = desiredAugerRPM;
        
        state = 93;
        directory = 38;
        break;
        
        
      case (93) :
        variable = 0;
        pullerRPM = encoderPos;
        pullerPulses = pullerRPM*100;
        puller.setSpeed(pullerPulses);
        //winder.setSpeed(pullerPulses/10-100);
        state = 94;
        directory = 39;
      
        break;
        
      case (94) :
        variable1 = 0;
        winderRPM = encoderPos;
        winderPulses = winderRPM*20;
        winder.setSpeed(winderPulses);
        state = 95;
        directory = 40;
        break;
        
        
      case (95) :
        variable3 = 0;
        fanSpeed = encoderPos*2.55;
        state = 0;
        directory = 0;
        fanState = !oldFanState;
        fanControl(fanSpeed);
        break;
        
        case (102) :
         state = 96;
         directory = 41;
         
        break;

   
    case (105) :
         state = 99;
         directory = 43;
         
        break;
        case (106) :
         state = 111;
         directory = 48;
         
        break;
        case (107) :
         state = 112;
         directory = 49;
         
        break;
        case (108) :
         state = 113;
         directory = 50;
         
        break;
        case (109) :
         state = 114;
         directory = 51;
         
         
        break;
        case (110) :
         state = 1;
         directory = 1;
         
        break;
       
        case (114) :
          state = 1;
          directory = 1;
        break;
        case (115) :
          mode = 1;
          state = 1;
          directory = 1;
          
          //heaterEnable = 1;
         //autoVariable = 1;
         //pullerReadyVariable = 1;
         //heaterVariable = 1;
         //diameterPID.SetMode(AUTOMATIC);
        break;
        case (116) :
          mode = 0;
          state = 1;
          directory = 1;
          //heaterEnable = 0;
         //autoVariable = 0;
         //pullerReadyVariable = 0;
         //diameterPID.SetMode(MANUAL);
         //pidStart = 0;
         
        break;
       
      case (126):
      state = 3;
      directory = 3;
      guideVariable = 1;
      break;
      case (127):
      state = 3;
      directory = 3;
      guideVariable = 0;
      break;
  }
    
    auger.runSpeed();
  puller.runSpeed();
  winder.runSpeed();
  guide.runSpeedToPosition();
  
 
  screenUpdate(state, currentTemp);
  //To ensure PLA Temp is correct
  if (state != 87) {
    encoderPos = 1;
  }
 }
}
  
