/* 
That is a file to configure all of Recyclebot features, like the type of the screen, reduction of your auger motor etc. 
In ideal case it is only file you'll need to modify to make you RecycleBot work properly

Electrical diagram can be found in electrical/scheme/quick.pdf file in this repo.

Right now I left whole pinout untouched in Main_Code_experimental.ino, because I think it is better to leave it there, without any changes, so you can use electrical scheme from this repo.
*/

#define TRUE 1 //LEAVE THAT UNCHANGED AS 1
#define FALSE 0 1 //LEAVE THAT UNCHANGED AS 0

/* Adjust the code at the bottom as you wish, but do not touch the code over this text - these are defines to make the code below more readable

Keep in mind that TRUE and FALSE words are case sensitive, so use only CAPITAL letters
 */



/* Change the value below to adjust the reduction value. For example our team had 18:1 reductor, so we put 18. Original RecyclebotV6 used 15:1, so in their case the line should look like: #define REDUCTION 15 */
#define REDUCTION 18


/* Arduino LCDs may have i2c converter, like we had. This makes the wiring simpler, becaue you need to connect only 4 wires, instead of 9 (or sth like this). As we tried to make the code as flexible as it is possible, right now you can use both LCD with and without converters. In case you are using LCD with i2c do not change anything. If you are using LCD without converter, change the value to: 

#define LCD_I2C FALSE  */

#define LCD_I2C TRUE //set to FALSE if not using i2c screen
